## Exercice 1.4.2 : Convertir un nombre en little endian
Compléter la fonction de l’exercice précédent pour convertir un nombre en sa notation variable little endian

Reprendre le programme de conversion de l’exercice précédent pour donner la notation hexadécimale en entier variable d’un nombre donné


Par exemple:

conversion(466321)

    → 0x 07 1d 91 (big endian)
	→ 0x 91 1d 07 (little endian)
	→ 0x fe 91 1d 07 00 (notation variable)
