const regexDecimal = /[0-9]+/;
const regexHexa = /([a-f]+\d*)|(\d*[a-f]+)/i;

//Conversion de nombres décimaux et hexa en little endian

const decimalToHex = (decimal) => {
    const hexNumber = [];
    while ( decimal !== 0 ) {
        const reste = Math.trunc(decimal / 16);
        let hex = decimal - (reste * 16);
        if ( hex > 9 )
            hex = String.fromCharCode(hex + 55);

        //console.log(hex);
        hexNumber.push(hex);
        decimal = Math.trunc(decimal / 16);
    }
    // console.log("hex : "+hexNumber.reverse().join(""));
    return hexNumber.reverse().join("");
}

function convertToLittleIndian(val) {
    try {
        if ( !regexDecimal.test(val) && !regexHexa.test(val) )
            throw new Error("La valeur à convertir doit être au format décimal ou hexadécimal");
        if ( val.match(regexHexa) ) {
            let valToConvert = (val.substr(0, 2) === "0x")
                ? val.substr(2) : val;
            if ( valToConvert.length % 2 !== 0 ) valToConvert = "0" + valToConvert;
            let valToConvertTab = [];
            for ( let i = 0; i < valToConvert.length; i += 2 ) {
                valToConvertTab.push(valToConvert.substr(i, 2));
            }
            return "0x"+valToConvertTab.reverse().join("").toUpperCase();
        } else {
            return convertToLittleIndian(decimalToHex(val));
        }
    } catch (e) {
        console.log(`Error : ${ e.message }`);
    }
}

console.log(convertToLittleIndian("fd8302"));
console.log(convertToLittleIndian("0xfFF34"));
console.log(convertToLittleIndian("123654"));
